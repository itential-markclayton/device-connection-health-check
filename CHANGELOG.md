
## 0.0.12 [04-15-2020]

* Update artifact.json, bundles/workflows/Device-Connection-Health-Check.json files

See merge request itentialopensource/pre-built-automations/device-connection-health-check!15

---

## 0.0.11 [04-15-2020]

* Update artifact.json, bundles/workflows/Device-Connection-Health-Check.json, manifest.json files

See merge request itentialopensource/pre-built-automations/device-connection-health-check!14

---

## 0.0.10 [04-15-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/device-connection-health-check!13

---

## 0.0.9 [04-15-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/device-connection-health-check!12

---

## 0.0.8 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/device-connection-health-check!10

---

## 0.0.7 [03-20-2020]

* removed tags from manifest

See merge request itentialopensource/pre-built-automations/device-connection-health-check!9

---

## 0.0.6 [03-10-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/device-connection-health-check!7

---

## 0.0.5 [01-14-2020]

* resize images and reformat document with markdown linting rules

See merge request itentialopensource/pre-built-automations/device-connection-health-check!3

---

## 0.0.4 [09-10-2019]

* patch/DSUP-713

See merge request itentialopensource/pre-built-automations/device-connection-health-check!2

---

## 0.0.3 [08-27-2019]

* Dsup 692

See merge request itentialopensource/pre-built-automations/device-connection-health-check!1

---

## 0.0.2 [08-27-2019]

* Dsup 692

See merge request itentialopensource/pre-built-automations/device-connection-health-check!1

---
